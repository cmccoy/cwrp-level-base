--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

util.AddNetworkString("Sublime.ExperienceNotification");

local SQL = Sublime.GetSQL();

---
--- AddExperience
---
function Sublime.Player:SL_AddExperience(amount, source, notify, shouldMultiply)
    local amount = (amount or 50);

    if (not isnumber(tonumber(amount))) then
        Sublime.Print("Argument 'Amount' in function SL_AddExperience needs to be a number.");

        return false;
    end

    shouldMultiply = shouldMultiply == nil and true or shouldMultiply;
    
    --if (player.GetCount() < 4) then
        --return false;
    --end

    local max = Sublime.Settings.Get("other", "max_level", "number");
    if (self:SL_GetLevel() >= max) then
        return;
    end

    local sData = Sublime.GetSkill("experienced_player");
    if (sData and sData.Enabled) then
        local points = self:SL_GetInteger(sData.Identifier, 0);

        if (points > 0 and shouldMultiply) then
            local modifier = 1 + ((points * sData.AmountPerPoint) / 100);

            amount = math.Round(amount * modifier);
        end
    end

    -- VIP bonus.
    local vModifier = Sublime.Settings.Get("other", "vip_modifier", "number");
    local isVip     = Sublime.Config.VipBonus[self:GetUserGroup()];

    if (isVip and shouldMultiply) then
        amount = amount * vModifier;
    end

    local new       = self:SL_GetInteger("experience", 0) + amount;
    local needed    = self:SL_GetNeededExperience();

    if (new >= needed) then
        self:SL_LevelUp();
    else
        self:SL_SetInteger("experience", new);
        self:SetNW2Int("sl_experience", new);
    end

    self.SL_ExperienceGained    = (self.SL_ExperienceGained and self.SL_ExperienceGained + amount) or amount;
    self.SL_ExperienceCount     = (self.SL_ExperienceCount and self.SL_ExperienceCount + 1) or 1;
    
    if (self.SL_ExperienceCount and self.SL_ExperienceCount and self.SL_ExperienceCount >= 5) then
        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Levels SET Experience = '%s', TotalExperience = TotalExperience + '%s' WHERE SteamID = '%s'", new, self.SL_ExperienceGained, self:SteamID64()));
        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Data SET ExperienceGained = ExperienceGained + '%s'", self.SL_ExperienceGained));

        self.SL_ExperienceGained    = 0;
        self.SL_ExperienceCount     = 0;

        Sublime.Print("Saved %s's experience.", self:Nick());
    end

    if (not source or source == "") then
        source = "from an unknown source.";
    end

    if (notify == nil) then
        notify = true;
    end

    if (notify) then
        net.Start("Sublime.ExperienceNotification")
            net.WriteString(source);
            net.WriteUInt(amount, 32);
        net.Send(self);
    end

    local left = new - needed;
    if (left > 0) then
        self:SL_AddExperience(left, source, notify, false);
    end

    hook.Run("SL.PlayerReceivedExperience", self, amount);

    return true;
end