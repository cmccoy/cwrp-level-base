--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

resource.AddWorkshop("1780556842");

util.AddNetworkString("Sublime.GetLeaderboardsData");
util.AddNetworkString("Sublime.SendLeaderboardsData");
util.AddNetworkString("Sublime.AdminAdjustData");
util.AddNetworkString("Sublime.ResetDatabase");

local SQL = Sublime.GetSQL();

---
--- SL_Save
---
function Sublime.Player:SL_Save()
    local level             = self:GetNW2Int("sl_level", 1);
    local experience        = self:SL_GetInteger("experience", 0);
    local ability_points    = self:SL_GetInteger("ability_points", 0);
    local steamid           = self:SteamID64();

    self.SL_ExperienceGained = self.SL_ExperienceGained == nil and 0 or self.SL_ExperienceGained;    

    Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Levels SET Level = '%s', Experience = '%s', TotalExperience = TotalExperience + '%s' WHERE SteamID = '%s'", level, experience, self.SL_ExperienceGained, steamid));
    Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Skills SET Points = '%s' WHERE SteamID = '%s'", ability_points, steamid));
    Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Data SET ExperienceGained = ExperienceGained + '%s'", self.SL_ExperienceGained));

    Sublime.Print("Saved %s's stats.", self:Nick());
end

net.Receive("Sublime.GetLeaderboardsData", function(_, ply)
    if (ply.SublimeLeaderboardsDataCooldown and ply.SublimeLeaderboardsDataCooldown > CurTime()) then
        return;
    end

    -- Current page.
    local cPage     = net.ReadUInt(32);
    local start    = 14 * (cPage - 1);
    local data      = sql.Query("SELECT SteamID, Level, Experience, TotalExperience, NeededExperience, Name FROM Sublime_Levels ORDER BY TotalExperience DESC");

    if (start == 0) then
        start = 1;
    end

    local players = {};
    for i = start, start + 14 do
        local pData = data[i];

        if (not pData) then
            continue;
        end

        table.insert(players, {
            position    = i,
            steamid     = pData.SteamID,
            level       = pData.Level,
            experience  = pData.Experience,
            total_xp    = pData.TotalExperience,
            needed_xp   = pData.NeededExperience,
            name        = pData.Name
        });
    end

    net.Start("Sublime.SendLeaderboardsData");
        net.WriteUInt(#players, 32);
        net.WriteUInt(math.ceil(#data / 14), 32);

        for i = 1, #players do
            local data = players[i];

            if (data) then
                net.WriteUInt(data.position, 32);
                net.WriteString(data.steamid);
                net.WriteUInt(data.level, 8);
                net.WriteUInt(data.experience, 32);
                net.WriteUInt(data.total_xp, 32);
                net.WriteUInt(data.needed_xp, 32);
                net.WriteString(data.name);
            end
        end
    net.Send(ply);
    
    ply.SublimeLeaderboardsDataCooldown = CurTime() + 5;
end);

local GIVE_LEVELS   = 0x1;
local TAKE_LEVELS   = 0x2;
local GIVE_SKILLS   = 0x3;
local TAKE_SKILLS   = 0x4;
local GIVE_XP       = 0x5;
local RESET_XP      = 0x6;

net.Receive("Sublime.AdminAdjustData", function(_, ply)
    if (not Sublime.Config.ConfigAccess[ply:GetUserGroup()]) then
        return;
    end

    local toAdjust  = net.ReadUInt(4);
    local value     = net.ReadUInt(32);
    local target    = net.ReadEntity();
    local steamid   = target:SteamID64();

    if (not IsValid(target)) then
        return;
    end

    if (toAdjust == GIVE_LEVELS) then
        local cLevel    = target:SL_GetLevel();
        local after     = cLevel + value;
        local max       = Sublime.Settings.Get("other", "max_level", "number");

        if (after > max) then
            return;
        end

        if (value < 1) then
            return;
        end

        target:SL_LevelUp(value);

        return;
    end

    if (toAdjust == TAKE_LEVELS) then
        local cLevel    = target:SL_GetLevel();
        local after     = cLevel - value;

        if (after < 0) then
            after = 0
        end

        target:SL_SetInteger("experience", 0);
        target:SetNW2Int("sl_level", after);
        target:SetNW2Int("sl_experience", 0);

        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Levels SET Level = '%s', Experience = '0', NeededExperience = '%s' WHERE SteamID = '%s'", after, target:SL_GetNeededExperience(), target:SteamID64()));

        Sublime.Print("%s has taken %i levels from %s.", ply:Nick(), value, target:Nick());

        return;
    end

    if (toAdjust == GIVE_SKILLS) then
        target:SL_AddSkillPoint(value);

        return;
    end

    if (toAdjust == TAKE_SKILLS) then
        local sPoints   = target:SL_GetInteger("ability_points", 0);
        local after     = sPoints - value;

        if (after < 0) then
            after = 0;
        end

        target:SL_SetInteger("ability_points", after);

        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Skills SET Points = '%s' WHERE SteamID = '%s'", after, target:SteamID64()));

        Sublime.Print("%s has now %i skill points to use.", target:Nick(), after);

        return;
    end

    if (toAdjust == GIVE_XP) then
        local cXP   = target:SL_GetInteger("experience", 0);
        local after = cXP + value;

        if (after >= target:SL_GetNeededExperience()) then
            target:SL_LevelUp();

            return;
        end

        target:SL_SetInteger("experience", after);
        target:SetNW2Int("sl_experience", after);

        target.SL_ExperienceGained  = (target.SL_ExperienceGained and target.SL_ExperienceGained + value) or value;

        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Levels SET Experience = '%s', TotalExperience = TotalExperience + '%s' WHERE SteamID = '%s'", after, target.SL_ExperienceGained, target:SteamID64()));
        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Data SET ExperienceGained = ExperienceGained + '%s'", target.SL_ExperienceGained));

        return;
    end

    if (toAdjust == RESET_XP) then
        target:SL_SetInteger("experience", 0);
        target:SetNW2Int("sl_experience", 0);

        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Levels SET Experience = '0' WHERE SteamID = '%s'", target:SteamID64()));

        target.SL_ExperienceGained    = 0;
        target.SL_ExperienceCount     = 0;

        Sublime.Print("%s has reset %s's experience.", ply:Nick(), target:Nick());

        return;
    end
end);

net.Receive("Sublime.ResetDatabase", function(_, ply)
    if (not ply:IsSuperAdmin()) then
        return;
    end

    sql.Query("DROP TABLE Sublime_Levels");
    sql.Query("DROP TABLE Sublime_Skills");
    sql.Query("DROP TABLE Sublime_Data");

    RunConsoleCommand("changelevel", game.GetMap());
end);