--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local path          = Sublime.GetCurrentPath();
local data          = {};
local panel         = {};
local panelRefrence = nil;
local maxPages      = 1;
local comma         = string.Comma;

-- Colors
local mainOffsetColor   = Color(34, 44, 44);
local otherOffsetColor  = Color(45, 105, 99);

---
--- AddPlayerToLeaderboard
---
function panel:AddPlayerToLeaderboard(parent, steamid, position, name, level, experience, needed, total_xp)
    local nextPlayer = #parent.Players + 1;

    parent.Players[nextPlayer] = parent:Add("DPanel");
    parent.Players[nextPlayer]:SetPos(5, 5 + 35 * (nextPlayer - 1));
    parent.Players[nextPlayer]:SetSize(parent:GetWide() - 10, 30);
    parent.Players[nextPlayer].PerformLayout = function(s, w, h)
        if (IsValid(s.Avatar)) then
            s.Avatar:SetPos(41, (h / 2) - s.Avatar.MaskSize / 2);
            s.Avatar:SetSize(s.Avatar.MaskSize, s.Avatar.MaskSize);
        end
    end
    
    parent.Players[nextPlayer].Paint = function(s, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, 50));

        Sublime:DrawTextOutlined(position, "Sublime.14", 5, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(name, "Sublime.14", 69, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(level, "Sublime.14", 250, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(comma(experience) .. "/" .. comma(needed), "Sublime.14", w - 334, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(comma(total_xp), "Sublime.14", w - 5, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_RIGHT, true);
    end

    parent.Players[nextPlayer].Avatar = parent.Players[nextPlayer]:Add("Sublime.AvatarCircleMask")
    parent.Players[nextPlayer].Avatar.MaskSize = 21;
	parent.Players[nextPlayer].Avatar:SetSteamID(steamid);
	parent.Players[nextPlayer].Avatar:SetMaskSize(parent.Players[nextPlayer].Avatar.MaskSize / 2);
end

---
--- FillLeaderboard
---
function panel:FillLeaderboard()
    local count = #data

    for i = 1, count do
        local pData = data[i];

        if (pData) then
            self:AddPlayerToLeaderboard(self.PlayerHolders, pData.steamid, pData.position, pData.name, pData.level, pData.experience, pData.needed_xp, pData.total_xp);
        end
    end
end

---
--- RebuildLeaderboard
---
function panel:RebuildLeaderboard()
    for i = 1, #self.PlayerHolders.Players do
        local panel = self.PlayerHolders.Players[i];

        if (IsValid(panel)) then
            panel:Remove();
        end
    end

    table.Empty(self.PlayerHolders.Players);
    self:FillLeaderboard();
end

---
--- RequestData
---
function panel:RequestData()
    net.Start("Sublime.GetLeaderboardsData")
        net.WriteUInt(self.Page, 32);
    net.SendToServer();

    self.Player.Sublime_LeaderCooldown = CurTime() + 5;
end

---
--- Init
---
function panel:Init()
    self.Page       = 1;
    self.Player     = LocalPlayer();
    self.ShouldFill = false;
    self.L          = Sublime.L;
    self.C          = Sublime.Colors;
    self.CA         = ColorAlpha;

    if (self.Player.Sublime_LeaderCooldown) then
        if (self.Player.Sublime_LeaderCooldown <= CurTime()) then
            self:RequestData();
        else
            self.ShouldFill = true;
        end
    else
        self:RequestData();
    end

    self.PlayerHolders = self:Add("DPanel");
    self.PlayerHolders.Players = {};
    self.PlayerHolders.Paint = function(panel, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(0, 0, 0, 100));
    end

    self.Next = self:Add("DButton");
    self.Next:SetText("");
    self.Next.Alpha = 100;
    self.Next.Paint = function(s, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, s.Alpha));

        surface.SetDrawColor(255, 255, 255);
        surface.SetMaterial(Sublime.Materials["SL_LeftArrow"]);
        surface.DrawTexturedRectRotated(w - 12, h / 2, 12, 12, 180);

        Sublime:DrawTextOutlined(self.L("leaderboards_next"), "Sublime.14", 5, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);

        if (s:IsHovered()) then
            s.Alpha = math.Approach(s.Alpha, 200, 4);
        else
            s.Alpha = math.Approach(s.Alpha, 100, 2);
        end
    end

    self.Next.DoClick = function()
        if (self.Page >= maxPages or self.Player.Sublime_LeaderCooldown > CurTime()) then
            return;
        else
            self.Page = self.Page + 1;
        end

        self:RequestData();
    end

    self.Next.OnCursorEntered = function()
        surface.PlaySound("sublime_levels/button.mp3");
    end

    self.Previous   = self:Add("DButton");
    self.Previous:SetText("");
    self.Previous.Alpha = 100;
    self.Previous.Paint = function(s, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, s.Alpha));

        surface.SetDrawColor(255, 255, 255);
        surface.SetMaterial(Sublime.Materials["SL_LeftArrow"]);
        surface.DrawTexturedRect(4, (h / 2) - 6, 12, 12);

        Sublime:DrawTextOutlined(self.L("leaderboards_previous"), "Sublime.14", w - 8, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER);

        if (s:IsHovered()) then
            s.Alpha = math.Approach(s.Alpha, 200, 4);
        else
            s.Alpha = math.Approach(s.Alpha, 100, 2);
        end
    end

    self.Previous.DoClick = function()
        if (self.Page <= 1 or self.Player.Sublime_LeaderCooldown > CurTime()) then
            return;
        else
            self.Page = self.Page - 1;
        end

        self:RequestData();
    end

    self.Previous.OnCursorEntered = function()
        surface.PlaySound("sublime_levels/button.mp3");
    end

    panelRefrence = self;
end

---
--- Think
---
function panel:Think()
    if (self.ShouldFill and next(data) and self.PlayerHolders:GetWide() > 64) then
        self:FillLeaderboard();

        self.ShouldFill = false;
    end
end

---
--- PerformLayout
---
function panel:PerformLayout(w, h)
    self.PlayerHolders:SetPos(5, 40);
    self.PlayerHolders:SetSize(w - 10, h - 80);

    self.Next:SetPos(w - 125, h - 35);
    self.Next:SetSize(120, 30);

    self.Previous:SetPos(5, h - 35);
    self.Previous:SetSize(120, 30);
end

---
--- Paint
---
function panel:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 0);
    surface.DrawRect(0, 0, w, h);

    draw.RoundedBox(8, 5, 5, w - 10, 30, Color(0, 0, 0, 100));

    Sublime:DrawTextOutlined("#", "Sublime.14", 15, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined(self.L("leaderboards_player"), "Sublime.14", 50, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined(self.L("leaderboards_level"), "Sublime.14", 250, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined(self.L("leaderboards_experience"), "Sublime.14", w - 345, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined(self.L("leaderboards_t_experience"), "Sublime.14", w - 15, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER);

    -- Leaderboard Pages.
    Sublime:DrawTextOutlined(self.Page .. "/" .. maxPages, "Sublime.14", (w / 2) - 7, h - 19, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER);
end
vgui.Register("Sublime.Leaderboards", panel, "EditablePanel");

hook.Add("Sublime.LeaderboardsDataRefreshed", path, function(newData, max)
    data = newData;
    maxPages = max;

    if (IsValid(panelRefrence)) then
        panelRefrence:RebuildLeaderboard();
    end
end);