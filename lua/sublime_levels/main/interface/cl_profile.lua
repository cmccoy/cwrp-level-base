--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local panel = {};
AccessorFunc(panel, "Player", "Player");

local GIVE_LEVELS   = 0x1;
local TAKE_LEVELS   = 0x2;
local GIVE_SKILLS   = 0x3;
local TAKE_SKILLS   = 0x4;
local GIVE_XP       = 0x5;
local RESET_XP      = 0x6; 

---
--- PostInit
---
function panel:PostInit()
    self:AddCMD(GIVE_LEVELS, "Give Levels", "Give Levels?", "How many levels do you want to give?", "Accept", true);
    self:AddCMD(TAKE_LEVELS, "Take Levels", "Take Levels?", "How many levels do you want to take? This doesn't take skill points.", "Accept", true);
    self:AddCMD(GIVE_SKILLS, "Give Skill Points", "Give Skill Points?", "How many skill points do you want to give?", "Accept", true);
    self:AddCMD(TAKE_SKILLS, "Take Skill Points", "Take Skill Points?", "How many skill points do you want to take?", "Accept", true);
    self:AddCMD(GIVE_XP, "Give Experience", "Give XP?", "You can only give whatever the person is missing for a Level or less.", "Accept", true);
    self:AddCMD(RESET_XP, "Reset Experience", "Reset XP?", "This sets his XP earned to 0.", "Accept", false);

    self.PlayerNick         = self.Player:Nick();
    self.PlayerSteamID      = self.Player:SteamID64();
    self.Level              = self.Player:SL_GetLevel();
    self.Experience         = self.Player:SL_GetExperience();
    self.Needed             = self.Player:SL_GetNeededExperience();
    self.PostInitCalled     = true;
end

---
--- AddCMD
---
function panel:AddCMD(id, name, noti_header, noti_desc, noti_accept, useTextEntry)
    useTextEntry = useTextEntry or false;

    local nextCMD = #self.Cmds + 1;
    local r = math.random;

    self.Cmds[nextCMD] = self:Add("DButton");
    self.Cmds[nextCMD].Alpha = 100;
    self.Cmds[nextCMD]:SetText("");
    self.Cmds[nextCMD].Text = name;
    self.Cmds[nextCMD].Color = Color(r(1, 255), r(1, 255), r(1, 255));
    self.Cmds[nextCMD].Paint = function(panel, w, h)
        local color1 = self.CA(Sublime:LightenColor(panel.Color, 50), panel.Alpha);
        local color2 = self.CA(Sublime:DarkenColor(panel.Color, 25), panel.Alpha);

        Sublime:DrawRoundedGradient(panel, 8, 0, 0, w, h, color1, color2);
        Sublime:DrawTextOutlined(name, "Sublime.16", 5, h / 2, self.C.White, self.C.Black, TEXT_ALIGN_LEFT, true);

        panel.Alpha = Sublime:DoHoverAnim(panel, panel.Alpha, {150, 2}, {100, 2});
    end

    self.Cmds[nextCMD].DoClick = function()
        local noti = Sublime.MakeNotification(noti_header, noti_desc, true, useTextEntry);
        noti.DoAcceptClick = function(s)
            if (useTextEntry) then
                local value = tonumber(noti:GetTextEntryValue());

                if (not isnumber(value) and useTextEntry) then
                    Sublime.MakeNotification("Invalid Character", "This field can only hold numbers.");

                    return false;
                end

                if (not IsValid(self.Player)) then
                    Sublime.MakeNotification("Opsie", "It seems that this player has left the server.");
                    
                    return;
                end

                if (id == GIVE_LEVELS) then
                    local currentLevel = self.Player:GetNW2Int("sl_level", 1);
                    local after = currentLevel + value;
                    local max = tonumber(Sublime.Settings.Table["SERVER"]["other"]["max_level"]);

                    if (after > max) then
                        Sublime.MakeNotification("Reached Max", "The max levels you can give this person is " .. max - currentLevel .. ". This is because else it would exceed the max level.");

                        return false;
                    end

                    if (value < 1) then
                        Sublime.MakeNotification("Invalid Value", "Number must be greater than 0. If you want to take levels then use the take option instead.");

                        return false;
                    end
                end

                if (id == RESET_XP) then
                    value = 0;
                end
                
                net.Start("Sublime.AdminAdjustData");
                    net.WriteUInt(id, 4);
                    net.WriteUInt(value, 32);
                    net.WriteEntity(self.Player);
                net.SendToServer();

                Sublime.MakeNotification("Success", "Your success to change " .. self.PlayerNick .. "'s data was accepted.");
            end
        end
    end
end

---
--- Init
---
function panel:Init()
    self.L      = Sublime.L;
    self.Cmds   = {};
    self.C      = Sublime.Colors;
    self.CA     = ColorAlpha;

    self:GetParent().CreatedPanel = self;
end

---
--- PerformLayout
---
function panel:PerformLayout(w, h)
    local combinedWidth = 0;
    local padding = 5;

    for i = 1, #self.Cmds do
        local pnl = self.Cmds[i];

        if (IsValid(pnl)) then
            surface.SetFont("Sublime.16");
            local contents = surface.GetTextSize(pnl.Text) + (padding * 2);

            pnl:SetPos(padding + combinedWidth, 40);
            pnl:SetSize(contents, 30);

            combinedWidth = combinedWidth + (contents + padding);
        end
    end
end

---
--- Think
---
function panel:Think()
    if (not self.PostInitCalled and IsValid(self.Player) and self:GetWide() > 64) then
        self:PostInit();
    end
end

---
--- Paint
---
function panel:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 0);
    surface.DrawRect(0, 0, w, h);

    draw.RoundedBox(8, 5, 5, w - 10, 30, Color(0, 0, 0, 100));

    Sublime:DrawTextOutlined(self.PlayerNick .. " (" .. (self.PlayerSteamID or "BOT") .. ") - Level: " .. self.Level .. " - Experience: " .. string.Comma(self.Experience) .. "/" .. string.Comma(self.Needed), "Sublime.14", 15, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
end
vgui.Register("Sublime.Profile", panel, "EditablePanel");

