--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local panel = {};
local path  = Sublime.GetCurrentPath();

---
--- Build
---
function panel:Build()
    /*self:CreateTextBox(self.L("server_chat_command"), "other", "chat_command", "string");
    self:CreateTextBox(self.L("server_kills_npc"), "kills", "npc_on_kill_experience", "number");
    self:CreateTextBox(self.L("server_kills_player"), "kills", "player_on_kill_experience", "number");
    self:CreateTextBox(self.L("server_kills_headshot"), "kills", "headshot_modifier", "number");
    self:CreateTextBox(self.L("server_other_vip"), "other", "vip_modifier", "number");
    self:CreateTextBox(self.L("server_other_max"), "other", "max_level", "number");
    self:CreateTextBox(self.L("server_other_xp_for_playing"), "other", "xp_for_playing", "number");
    self:CreateTextBox(self.L("server_other_xp_when"), "other", "xp_for_playing_when", "number");

    if (DarkRP) then
        self:CreateTextBox(self.L("server_darkrp_lottery"), "darkrp", "lottery_winner", "number");
        self:CreateTextBox(self.L("server_darkrp_hitman"), "darkrp", "hitman_completed", "number");
        self:CreateTextBox(self.L("server_darkrp_arrested"), "darkrp", "player_arrested", "number");
        self:CreateTextBox(self.L("server_darkrp_taxed"), "darkrp", "player_taxed", "number");
    end

    if (GAMEMODE and GAMEMODE.ThisClass and GAMEMODE.ThisClass:find("terrortown")) then
        self:CreateTextBox(self.L("server_ttt_traitor_win"), "ttt", "traitor_winners", "number");
        self:CreateTextBox(self.L("server_ttt_innocent_win"), "ttt", "innocent_winners", "number");
        self:CreateTextBox(self.L("server_ttt_draw_win"), "ttt", "draw", "number");
    end

    if (GAMEMODE and GAMEMODE.ThisClass and GAMEMODE.ThisClass:find("murder")) then
        self:CreateTextBox("How much experience should a Murderer get when he wins the round?", "murder", "murderer_winners", "number");
        self:CreateTextBox("How much experience should a Bystander get when he wins the round?", "murder", "bystander_winners", "number");
    end

    self:CreateTextBox(self.L("server_other_debug"), "other", "debug_enabled", "string");*/
end

---
--- Init
---
function panel:Init()
    self.Player         = LocalPlayer();
    self.SettingsCopy   = Sublime.Settings.Table["SERVER"];

    self.L  = Sublime.L;
    self.C  = Sublime.Colors;
    self.CA = ColorAlpha;

    self.Items = {};

    self.ScrollPanel = self:Add("DScrollPanel");
    local vBar = self.ScrollPanel:GetVBar();

    vBar:SetHideButtons(true);
    
    vBar.Color = Color(0, 0, 0, 50);
    vBar.Paint = function(panel, w, h)
        draw.RoundedBox(8, 2, 0, w - 4, h, panel.Color);    
    end

    vBar.btnGrip.Alpha = 25;
    vBar.btnGrip.Paint = function(panel, w, h)
        draw.RoundedBox(8, 2, 0, w - 4, h, self.C.Outline);

        panel.Alpha = Sublime:DoHoverAnim(panel, panel.Alpha, {75, 2}, {25, 2}, panel:GetParent().Dragging); 
    end

    self:CreateSetting(self.L("server_chat_command"), "other", "chat_command", "string");
    self:CreateSetting(self.L("server_kills_npc"), "kills", "npc_on_kill_experience", "number");
    self:CreateSetting(self.L("server_kills_player"), "kills", "player_on_kill_experience", "number");
    self:CreateSetting(self.L("server_kills_headshot"), "kills", "headshot_modifier", "number");
    self:CreateSetting(self.L("server_other_vip"), "other", "vip_modifier", "number");
    self:CreateSetting(self.L("server_other_max"), "other", "max_level", "number");
    self:CreateSetting(self.L("server_other_xp_for_playing"), "other", "xp_for_playing", "number");
    self:CreateSetting(self.L("server_other_xp_when"), "other", "xp_for_playing_when", "number");
    self:CreateSetting(self.L("server_other_level_up_sound"), "other", "sound_on_level", "string");
    self:CreateSetting(self.L("server_other_experience_sound"), "other", "sound_on_xp", "string");

    if (DarkRP) then
        self:CreateSetting(self.L("server_darkrp_lottery"), "darkrp", "lottery_winner", "number");
        self:CreateSetting(self.L("server_darkrp_hitman"), "darkrp", "hitman_completed", "number");
        self:CreateSetting(self.L("server_darkrp_arrested"), "darkrp", "player_arrested", "number");
        self:CreateSetting(self.L("server_darkrp_taxed"), "darkrp", "player_taxed", "number");
    end

    if (GAMEMODE and GAMEMODE.ThisClass and GAMEMODE.ThisClass:find("terrortown")) then
        self:CreateSetting(self.L("server_ttt_traitor_win"), "ttt", "traitor_winners", "number");
        self:CreateSetting(self.L("server_ttt_innocent_win"), "ttt", "innocent_winners", "number");
        self:CreateSetting(self.L("server_ttt_draw_win"), "ttt", "draw", "number");
    end

    if (GAMEMODE and GAMEMODE.ThisClass and GAMEMODE.ThisClass:find("murder")) then
        self:CreateSetting("How much experience should a Murderer get when he wins the round?", "murder", "murderer_winners", "number");
        self:CreateSetting("How much experience should a Bystander get when he wins the round?", "murder", "bystander_winners", "number");
    end

    self.Save = self:Add("DButton");
    self.Save.Alpha = 50;
    self.Save:SetText("");
    self.Save.Paint = function(panel, w, h)
        local color1 = self.CA(Sublime:LightenColor(self.C.Green, 50), panel.Alpha);

        draw.RoundedBox(8, 0, 0, w, h, color1);
        Sublime:DrawTextOutlined(self.L("server_save"), "Sublime.14", w / 2, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_CENTER, true);
    
        panel.Alpha = Sublime:DoHoverAnim(panel, panel.Alpha, {100, 2}, {50, 2});
    end

    self.Save.DoClick = function()
        net.Start("Sublime.SaveServerSideSettings");
            net.WriteTable(Sublime.Settings.Table["SERVER"]);
        net.SendToServer();
    end

    self.Reset = self:Add("DButton");
    self.Reset.Alpha = 50;
    self.Reset:SetText("");
    self.Reset.Paint = function(panel, w, h)
        local color1 = self.CA(Sublime:LightenColor(self.C.Red, 50), panel.Alpha);

        draw.RoundedBox(8, 0, 0, w, h, color1);
        Sublime:DrawTextOutlined(self.L("server_default"), "Sublime.14", w / 2, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_CENTER, true);
    
        panel.Alpha = Sublime:DoHoverAnim(panel, panel.Alpha, {100, 2}, {50, 2});
    end

    self.Reset.DoClick = function()
        net.Start("Sublime.ResetServerSettings");
        net.SendToServer();
    end
end

function panel:CreateSetting(title, category, key, type)
    local nextSetting = #self.Items + 1;

    self.Items[nextSetting] = self.ScrollPanel:Add("DPanel");
    local item = self.Items[nextSetting];

    item.Paint = function(panel, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(0, 0, 0, 100));

        Sublime:DrawTextOutlined(title, "Sublime.16", 5, 15, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
    end

    item.PerformLayout = function(panel, w, h)
        item.TextEntry:SetPos(5, h - 35);
        item.TextEntry:SetSize(100, 30)
    end

    item.TextEntry = item:Add("DTextEntry");
    item.TextEntry:SetDrawLanguageID(false);
    item.TextEntry:SetFont("Sublime.16")
    item.TextEntry.Paint = function(panel, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, 100));
        panel:DrawTextEntryText(self.C.White, self.C.Grey, self.C.Grey);
    end

    item.TextEntry.OnTextChanged = function(panel)
        local value = panel:GetValue();

        if (type == "number") then
            value = tonumber(value);

            if (not isnumber(value)) then
                return;
            end
        end

        Sublime.Settings.Table["SERVER"][category][key] = value;
    end

    item.TextEntry:SetValue(Sublime.Settings.Table["SERVER"][category][key]);
end

---
--- Think
---
function panel:Think()
end

---
--- PostInit
---
function panel:PostInit()
end

---
--- PerformLayout
---
function panel:PerformLayout(w, h)
    self.ScrollPanel:SetPos(0, 0);
    self.ScrollPanel:SetSize(w, h - 40)

    self.Save:SetPos(5, h - 35);
    self.Save:SetSize((w / 2) - 5, 30);

    self.Reset:SetPos((w / 2) + 5, h - 35);
    self.Reset:SetSize((w / 2) - 20, 30);

    for i = 1, #self.Items do
        local panel = self.Items[i];

        panel:SetPos(5, 5 + (65 * (i - 1)));
        panel:SetSize(w - 10, 60);
    end
end

---
--- Paint
---
function panel:Paint(w, h)
end
vgui.Register("Sublime.OptionsServer", panel, "EditablePanel");